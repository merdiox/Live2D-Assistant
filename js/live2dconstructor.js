// Add event for background music
//var checkbox_music = document.getElementById("backgroundmusic");
//const audio = new Audio("assets/audio/shuqing.mp3");

/*checkbox_music.addEventListener("change", function() {
  if (checkbox_music.checked) {
    audio.play();
    audio.loop = true;
  } else {
    audio.pause();
  }
});*/
// Events to draw model + setup settings
var fixed_scale = 0;
/* document.addEventListener('DOMContentLoaded', () => {
  // Wait for the IPC event to set the value
  electron.ipcRenderer.on('return-cursor-position', (event, position) => {
      console.log('Global position in other.js:', window.globalPosition);
      
  });

}); */

const {
  Application,
  live2d: { Live2DModel, config },
} = PIXI;
let x = 0;
let y = 0;


async function render({ usePixiContainer, canvasEl, parentEl, motionarray, checked_val }) {

  const app = new Application({
    view: canvasEl,
    autoStart: true,
    transparent: true,
    backgroundAlpha: 0,
    resizeTo: parentEl,
  });

  // Clear any prior model displayed
  app.stage.removeChildren();
  const filePath = 'config.json';
  const config = await window.nodeModules.readJsonFile(filePath);
  const modelPath = config.configData.filePath;
  const modelDirectory = window.nodeModules.path.dirname(modelPath);
  const positionPath = window.nodeModules.path.join(modelDirectory, 'model_position.json')
  const modelPosition = await window.nodeModules.readJsonFile(positionPath);
  const modelInfo = await window.nodeModules.readJsonFile(modelPath);
  const motionList = modelInfo.FileReferences.Motions
  const motionKeys = Object.keys(motionList)
  const motion = {};

for (const key of motionKeys) {
  motion[key] = key;
}

  const currentModel = await Live2DModel.from(modelPath, { idleMotionGroup: 'idle' });
  const motion_name = 'idle';

  // Default model scale
  currentModel.scale.set(0.1);

  // Adjust breath parameter
  currentModel.internalModel.breath._breathParameters[0]['peak'] = 30;
  currentModel.internalModel.breath._breathParameters[1]['peak'] = 16;
  currentModel.internalModel.breath._breathParameters[2]['peak'] = 20;
  currentModel.internalModel.breath._breathParameters[3]['peak'] = 20;
  currentModel.internalModel.breath._breathParameters[4]['peak'] = 1.5;

  async function getAndLogScreenInfo() {
    const screenInfo = await electron.getScreenInfo();
    currentModel.focus(screenInfo.x, screenInfo.y);
  }
  
  // Call the function immediately
  getAndLogScreenInfo();
  setInterval(getAndLogScreenInfo, 10);
  config.idleMotionFadingDuration = 0;

  var container = document.getElementById('button-container')
  function calculateContainerPosition(x, y) {
    var containerWidth = container.offsetWidth;
    var containerHeight = container.offsetHeight;
    const paddingWidth = window.innerWidth * 0.25; // 25% padding on each side horizontally
    const paddingHeight = window.innerHeight * 0.2; // 25% padding on each side vertically

    // Calculate the maximum left and top positions to keep the container within the centered padding.
    const maxLeftPosition = paddingWidth;
    const maxTopPosition = paddingHeight;

    // Calculate the minimum left and top positions to keep the container within the centered padding.
    const minLeftPosition = window.innerWidth - paddingWidth;
    const minTopPosition = window.innerHeight - paddingHeight;

    // Calculate the left and top positions to keep the container within the boundaries.
    let leftPosition = x;
    let topPosition = y;

    // Check if the container exceeds the right boundary.
    if (x + containerWidth > minLeftPosition) {
        leftPosition = minLeftPosition - containerWidth;
    }

    // Check if the container exceeds the bottom boundary.
    if (y + containerHeight > minTopPosition) {
        topPosition = minTopPosition - containerHeight;
    }

    // Ensure the left and top positions do not go beyond the left and top boundaries.
    leftPosition = Math.max(leftPosition, maxLeftPosition);
    topPosition = Math.max(topPosition, maxTopPosition);

    return { left: leftPosition + 'px', top: topPosition + 'px' };
}


  // Add events to drag model
  currentModel.interactive = true;
  currentModel.anchor.set(0.5, 0.5);
  currentModel.position.set(parentEl.offsetWidth * 0.5, parentEl.offsetHeight * 0.5);

  var newPosition = calculateContainerPosition(modelPosition.x, modelPosition.y);
  container.style.left = newPosition.left;
  container.style.top = newPosition.top;  
  const x = modelPosition.x || 800;    // Use modelPosition.x if available, otherwise use defaultX
  const y = modelPosition.y || 800;
  const th = modelPosition.angle || 0;    // Use modelPosition.y if available, otherwise use defaultY
  const zoom = modelPosition.zoom || 0.15;   // Use modelPosition.zoom if available, otherwise use defaultZoom
  
  currentModel.position.set(x, y);
  currentModel.rotation=th
  currentModel.scale.set(zoom);

  currentModel.on('pointerdown', (e) => {
    currentModel.offsetX = e.data.global.x - currentModel.position.x;
    currentModel.offsetY = e.data.global.y - currentModel.position.y;
    currentModel.dragging = true;
  });

  currentModel.on('pointerup', (e) => {
    currentModel.dragging = false;
  });

  currentModel.on('pointermove', (e) => {
    if (currentModel.dragging) {
      const newX = e.data.global.x - currentModel.offsetX;
      const newY = e.data.global.y - currentModel.offsetY; 
      // Update the model's position
      currentModel.position.set(newX, newY)
      var newPosition = calculateContainerPosition(newX, newY);
      container.style.left = newPosition.left;
      container.style.top = newPosition.top;  
    }
  });
  
  currentModel.on('wheel', (e) => {
    currentModel.scale.set(currentModel.scale.x - e.deltaY * 0.0001);
    currentModel.scale.x = Math.min(Math.max(0.02, currentModel.scale.x), 0.6);
    fixed_scale = currentModel.scale.x;
    rounded_scale = fixed_scale.toFixed(3)
  });
  electron.ipcRenderer.on('save-pos', (event) => {
    dataToSave=[currentModel.position.x,currentModel.position.y,currentModel.scale.x]
    electron.ipcRenderer.send('write-json', [dataToSave, modelPath])
  });

  // Add mouse motion tracking (disable on mobile)
  if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    
    $('#mousetrack').click(function () {
      //console.log(this.checked);
      checked_val = this.checked;
    });
	$(".selectMotion").change(function () {
		let selectedMotion = $(this).val();
	  
		// Stop the current motion
		app.stage.children[0].internalModel.motionManager.stopAllMotions();
	  
		// Play the selected motion
		app.stage.children[0].internalModel.motionManager.startMotion(selectedMotion, 0);
	  });
	  function selectRandomMotion() {
		const availableMotions = Object.keys(motion);
    console.log(availableMotions)
		const randomIndex = Math.floor(Math.random() * availableMotions.length);
		return availableMotions[randomIndex];
	  }
	  
	  // Add click event listener to the Live2D model
	  currentModel.on('pointertap', () => {
		// Stop the current motion
		app.stage.children[0].internalModel.motionManager.stopAllMotions();
	  
		// Select and play a random motion
		const randomMotion = selectRandomMotion();
		app.stage.children[0].internalModel.motionManager.startMotion(randomMotion, 0);
	  });

    


    function playRandomMotionEvery15Seconds() {
      setInterval(() => {
        const randomMotion = selectRandomMotion();
        app.stage.children[0].internalModel.motionManager.stopAllMotions();
        app.stage.children[0].internalModel.motionManager.startMotion(randomMotion, 0);
        //console.log(`Playing motion: ${randomMotion}`);
        return randomMotion
      }, 180000); // 15000 milliseconds = 15 seconds
    }

    playRandomMotionEvery15Seconds();

  }

  // Draw model on screen
  app.stage.addChild(currentModel);

  // Play the idle motion by default
  app.stage.children[0].internalModel.motionManager.startMotion(motion_name, 0);
  electron.ipcRenderer.send('model-ready')
}

const initialCheckedVal = true;


  render({
    usePixiContainer: false,
    canvasEl: document.getElementById('canvas-1'),
    parentEl: document.getElementById('container-1'),
    checked_val: initialCheckedVal, // Pass the initial checked_val value
  });


