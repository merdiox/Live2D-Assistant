// preload.js
const { contextBridge, ipcRenderer, screen } = require('electron');
const path = require ('node:path')
const fs = require('node:fs/promises');
const cmd = require('node-cmd');
console.log("Screen module available:", typeof screen !== 'undefined');


contextBridge.exposeInMainWorld("electron", {
    ipcRenderer: {
      ...ipcRenderer,
      on: ipcRenderer.on.bind(ipcRenderer),
      removeListener: ipcRenderer.removeListener.bind(ipcRenderer),
    },
    getScreenInfo: async () => {
      try {
        const screenInfo = await ipcRenderer.invoke('getScreenInfo');
        return screenInfo;
      } catch (error) {
        console.error('Error getting screen information:', error);
        return null;
      }
    },
  });
  contextBridge.exposeInMainWorld('nodeModules', {
    async readJsonFile(filePath) {
      try {
        const configData = await fs.readFile(path.join(__dirname, filePath), 'utf-8');
        return JSON.parse(configData);
      } catch (error) {
        // Handle any errors that occur during file reading or JSON parsing
        console.error('Error reading config file:', error);
        return null;
      }
    },
    async updateJsonFile(filePath, newData) {
      try {
        // Read the existing JSON data from the file
        const existingData = await fs.readFile(filePath, 'utf-8');
        const parsedData = JSON.parse(existingData);
    
        // Merge the new data with the existing data
        const updatedData = { ...parsedData, ...newData };
    
        // Write the updated data back to the file
        await fs.writeFile(filePath, JSON.stringify(updatedData, null, 2), 'utf-8');
    
        console.log('Data updated successfully.');
      } catch (error) {
        console.error('Error updating JSON file:', error);
      }
    },
    path: path
  });
  contextBridge.exposeInMainWorld('nodeCmd', {
    run: (command, callback) => {
      cmd.run(command, callback);
    },
    // You can also expose `runSync` and other methods if you use them
  });
