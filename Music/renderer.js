const { ipcRenderer, dialog } = require('electron');
const fs = require('fs').promises;
const path = require('path');

const audio = document.getElementById('audio');
//const playPauseButton = document.getElementById('playPause');
const shuffleButton = document.getElementById('shuffle');
const playlist = document.getElementById('playlist');

let playlistData = []; // Array to store song data

let configData = {};

async function loadConfig() {
    try {
        filePath = path.join(__dirname,'..', 'config.json')
        const configFile = await fs.readFile(filePath, 'utf-8');
        console.log("Read config file:", configFile);
        configData = JSON.parse(configFile);
        console.log("Parsed config data:", configData);

        // Load the playlist from the saved folder path, if available
        if (configData.selectedFolderPath) {
            await populatePlaylist(configData.selectedFolderPath);
        }
    } catch (error) {
        console.error('Error loading config:', error);
    }
}

// Call this function when your application starts
loadConfig();

function createPlaylistItem(song, index) {
    const title = path.basename(song.fullPath, path.extname(song.fullPath));
    const item = document.createElement('div');
    item.textContent = `${index + 1}. ${title}`;
    item.addEventListener('click', () => playSongByPath(song.relativePath)); // Play the song when clicked
    return item;
}



function playSongByPath(relativePath) {
    if (playlistData.length === 0) return;

    const song = playlistData.find(song => song.relativePath === relativePath);
    if (song) {
        audio.src = song.fullPath;
        audio.play();
        //playPauseButton.textContent = 'Pause';
    }
}
let isShuffleMode = false;

function playRandomSong() {
    console.log(playlistData.length)
    if (playlistData.length === 0) return;
    
    const randomIndex = Math.floor(Math.random() * playlistData.length);
    playSongByPath(playlistData[randomIndex].relativePath);
}

shuffleButton.addEventListener('click', () => {
    isShuffleMode = true;  // Activate shuffle mode
    playRandomSong();
});

audio.addEventListener('ended', () => {
    if (isShuffleMode) {
        playRandomSong();
    }
});

playlist.addEventListener('click', () => {
    isShuffleMode = false;  // Deactivate shuffle mode
});

async function readDirRecursively(folderPath, relativePath = '') {
    const entries = await fs.readdir(folderPath, { withFileTypes: true });
    const audioFiles = [];
    for (const entry of entries) {
        const fullPath = path.join(folderPath, entry.name);
        const newRelativePath = path.join(relativePath, entry.name);
        if (entry.isDirectory()) {
            const nestedFiles = await readDirRecursively(fullPath, newRelativePath);
            audioFiles.push(...nestedFiles);
        } else {
            const ext = path.extname(entry.name).toLowerCase();
            if (['.mp3', '.wav', '.ogg'].includes(ext)) {
                audioFiles.push({
                    fullPath,
                    relativePath: newRelativePath
                });
            }
        }
    }
    return audioFiles;
}

// Function to populate the playlist
async function populatePlaylist(folderPath) {
    try {
        console.log('Config data before update:', configData);
        configData.selectedFolderPath = folderPath; // Ensure folderPath is a string

        await fs.writeFile('./config.json', JSON.stringify(configData, null, 4), 'utf-8');
        console.log('Config data after update:', configData);

        const audioFiles = await readDirRecursively(folderPath);

        // Update playlistData
        playlistData = audioFiles.map(({ fullPath, relativePath }) => ({
            fullPath,
            relativePath
        }));

        // Clear existing playlist items
        playlist.innerHTML = '';

// Populate the playlist with new items
playlistData.forEach((song, index) => {
    const item = createPlaylistItem(song, index);
    playlist.appendChild(item);
});
ipcRenderer.send('playlist-populated');
    } catch (err) {
        console.error('Error writing to config.json:', err);
    }

}


const browseButton = document.getElementById('browseButton');
// Upon clicking the "Browse" button, request the file from the main process
browseButton.addEventListener('click', () => {
  ipcRenderer.send('file-request');
});

// Upon receiving a file, process accordingly
ipcRenderer.on('file', async (event, file) => {
  await populatePlaylist(file);
});

ipcRenderer.on('shuffle-command', () => {
    console.log("ok")
    isShuffleMode = true;  // Activate shuffle mode
    playRandomSong();
});

ipcRenderer.on('skip-command', () => {
    if (playlistData.length === 0) return;
    if (isShuffleMode) {
        playRandomSong();  // Play a random song if in shuffle mode
    } else {
        // Implement logic to play the next song in the playlist in sequential order
    }
});

// Call the function when needed (e.g., by clicking a "Browse" button)
