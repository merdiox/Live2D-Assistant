const { createChatCompletion, createChatCompletionSimple } = require('./api');

let chats = {};
let currentChatId = null;

const path = require('path');
const fs = require('fs');


function readConfigJson() {
    try {
        const configPath = path.join(__dirname, '..', 'config.json');
        const configData = fs.readFileSync(configPath, 'utf8');
        const config = JSON.parse(configData);

        const modelPositionPath = path.join(__dirname, '..',path.join(path.dirname(config.configData.filePath), 'model_position.json'));
        console.log(modelPositionPath)
        const modelPositionData = fs.readFileSync(modelPositionPath, 'utf8');
        const configJson = JSON.parse(modelPositionData);

        return configJson;
    } catch (error) {
        console.error("An error occurred while reading config: ", error);
    }
}

const modelColors = {
    'gpt-3.5-turbo-16k-0613': 'rgba(0, 166, 126, 0.75)', // Transparent green
    'gpt-4': 'rgba(171, 104, 255, 0.75)',      // Transparent purple
    // Add more model-transparentColor pairs as needed
};

// Function to assign colors to chat sessions based on their models
function assignColorsToChatSessions() {
    const chatSessions = document.getElementById('chat-sessions');
    
    for (const chatId in chats) {
        const chatSession = chats[chatId];
        const model = chatSession.model;
        const chatButton = document.getElementById(`chat-button-${chatId}`);
        
        if (modelColors[model]) {
            const color = modelColors[model];
            chatButton.style.backgroundColor = color;
        }
    }
}
function shiftColor(color, shiftValue) {
    // Check if the color has an alpha channel (RGBA format)
    const isRgba = color.startsWith('rgba');
    
    // Extract the RGB and alpha values
    let rgbValues, alpha;
    
    if (isRgba) {
        const rgbaValues = color.match(/[\d.]+/g);
        rgbValues = rgbaValues.slice(0, 3); // Extract RGB values (r, g, b)
        alpha = rgbaValues[3]; // Extract alpha value
    } else if (color.startsWith('rgb')) {
        rgbValues = color.match(/\d+/g); // Extract RGB values (r, g, b)
        alpha = 1.0; // Set default alpha value if not provided
    } else {
        return color; // Return the original color if it's not recognized
    }
    
    // Parse RGB values as integers
    const r = parseInt(rgbValues[0]);
    const g = parseInt(rgbValues[1]);
    const b = parseInt(rgbValues[2]);

    // Calculate shifted RGB values (within the [0, 255] range)
    const shiftedR = Math.max(0, Math.min(255, r + shiftValue));
    const shiftedG = Math.max(0, Math.min(255, g + shiftValue));
    const shiftedB = Math.max(0, Math.min(255, b + shiftValue));
    
    // Reconstruct the color with the same alpha value
    if (isRgba) {
        return `rgba(${shiftedR}, ${shiftedG}, ${shiftedB}, ${alpha})`;
    } else {
        return `rgb(${shiftedR}, ${shiftedG}, ${shiftedB})`;
    }
}

const MarkdownIt = require('markdown-it');
const markdownItSanitizer = require('markdown-it-sanitizer');
const md = new MarkdownIt().use(markdownItSanitizer);

function displayMessage(role, messageContent, chatId) {
    const chatContainer = document.getElementById('chat-container');
    const messageElement = document.createElement('div');
    messageElement.classList.add('message');
  
    // Create message image element
    const imgElement = document.createElement('img');
    imgElement.src = role === 'user' ? 'user.png' : 'assistant.png';
    imgElement.classList.add('message-img');
  
    // Create message text element
    const messageText = document.createElement('div');
    messageText.classList.add('message-text');
  
    // Convert markdown to HTML
    const htmlContent = md.render(messageContent);
    messageText.innerHTML = htmlContent;
  
    // Append both image and text to the message element
    messageElement.appendChild(imgElement);
    messageElement.appendChild(messageText);
  
    chatContainer.appendChild(messageElement);
    chatContainer.scrollTop = chatContainer.scrollHeight;
  }


function updateLastMessage(chatId, updatedMessage) {
    const chatContainer = document.getElementById('chat-container');
    if (!chatContainer) return;

    // Get the last message element in the chat container
    const lastMessageElement = chatContainer.lastElementChild;

    if (lastMessageElement) {
        // Get the <div> element containing the message text
        const messageTextElement = lastMessageElement.querySelector('div');
        if (messageTextElement) {
            // Render the updated message content using the Markdown parser
            const renderedContent = md.render(updatedMessage.content);

            // Update the innerHTML of the message text element with the rendered content
            messageTextElement.innerHTML = renderedContent;
        }
    }
    chatContainer.scrollTop = chatContainer.scrollHeight;
}
function updateChatSessions(chatId) {
    const chatSessions = document.getElementById('chat-sessions');
    let chatButton = document.getElementById(`chat-button-${chatId}`);

    if (!chatButton) {
        chatButton = document.createElement('button');
        chatButton.id = `chat-button-${chatId}`;
        chatButton.textContent = chatId;
        chatButton.addEventListener('click', () => {
            assignColorsToChatSessions(); // Reset colors of all buttons
            chatButton.style.backgroundColor = shiftColor(chatButton.style.backgroundColor, -30);
            switchChatSession(chatId);
        });
            
        // Add delete icon to chat session button
        const deleteIcon = document.createElement('span');
        deleteIcon.classList.add('delete-icon');
        deleteIcon.innerHTML = '&#x1F5D1;'; // Trash can emoji
        deleteIcon.addEventListener('click', (event) => {
            event.stopPropagation(); // Prevent switching to the chat session when the icon is clicked
            deleteChatSession(chatId);
        });

        chatButton.appendChild(deleteIcon);

        // Prepend the chat button to the beginning of the chat-sessions container
        chatSessions.insertBefore(chatButton, chatSessions.firstChild);
    }
}


function deleteChatSession(chatId) {
    // Show a confirmation popup
    const confirmDeletion = confirm('Are you sure you want to delete this chat session?');

    if (!confirmDeletion) {
        return;
    }

    const chatSessions = document.getElementById('chat-sessions');
    const chatButton = document.getElementById(`chat-button-${chatId}`);

    if (chatButton) {
        // Remove the chat button from the chat sessions list
        chatSessions.removeChild(chatButton);
    }

    // Remove the chat session from the chats object
    delete chats[chatId];

    // If the deleted chat session is the current one, switch to the first available chat session
    if (chatId === currentChatId) {
        const firstChatId = Object.keys(chats)[0];
        switchChatSession(firstChatId);
    }
}

function switchChatSession(chatId, modelColor) {
    const chatSession = chats[chatId];

    if (!chatSession) return;

    // Set the current chat ID
    currentChatId = chatId;

    // Clear the chat container
    const chatContainer = document.getElementById('chat-container');
    chatContainer.innerHTML = '';

    // Apply the model color to the HTML element
    const modelColorElement = document.getElementById('user-input'); // Replace 'model-color' with the actual ID of your element
    modelColorElement.style.backgroundColor = modelColor;

    // Add messages from the selected chat session
    for (const message of chatSession.messages) {
        if (message.role === 'system') {
            continue;
        }
        displayMessage(message.role, message.content, chatId);
    }
}

function createNewChatSession() {
    const configJson = readConfigJson();
    const modelSelect = document.getElementById('model-select');
    const chatId = `chat-${Date.now()}`;
    const model = modelSelect.value;

    chats[chatId] = {
        model: model,
        messages: [{ "role": "system", "content": configJson.personnality || "You are a helpful assistant" }],
    };

    saveChatsToLocalStorage();
    currentChatId = chatId;
    updateChatSessions(chatId);
    switchChatSession(chatId);

    // Assign colors to chat sessions
    assignColorsToChatSessions();

    // Add event listeners for hover and click events

}

function addUserMessageToChat(message) {
    const chatSession = chats[currentChatId];
    if (!chatSession) return;

    chatSession.messages.push({ role: 'user', content: message });
    saveChatsToLocalStorage(); // Add this line
    displayMessage('user', message, currentChatId);
}

const MAX_TOKENS = 16000;
const CHARACTERS_PER_TOKEN = 3; // Adjust this based on your observations

async function addAssistantMessageToChat(message) {
    console.log(message)
    const chatSession = chats[currentChatId];
    if (!chatSession) return;

    const model = chatSession.model;
    const messages = chatSession.messages;
    console.log(messages)
    console.log(estimateTokens(messages) + (message.length / CHARACTERS_PER_TOKEN))

    // Check for potential token overflow
    while ((estimateTokens(messages) + (message.length / CHARACTERS_PER_TOKEN)) > MAX_TOKENS) {
        if (messages.length > 1) {
            messages.splice(1, 2); // Remove the second oldest message, preserving the prompt
        } else {
            break; // Just the prompt remains. Ideally, this condition shouldn't be reached, but it's a safety check.
        }
    }

    let consolidatedMessage = null;

    await createChatCompletion(model, messages, (assistantMessage) => {
        if (assistantMessage) {
            // Add this check here
            const lastMessage = chatSession.messages[chatSession.messages.length - 1];
            
            if (lastMessage && lastMessage.content !== assistantMessage) {
                saveChatsToLocalStorage();
    
                if (lastMessage.role === 'assistant' || lastMessage.role === 'system') {
                    lastMessage.content += assistantMessage;
                    updateLastMessage(currentChatId, lastMessage);
                } else {
                    const newAssistantMessage = { role: 'assistant', content: assistantMessage };
                    chatSession.messages.push(newAssistantMessage);
                    displayMessage('assistant', assistantMessage, currentChatId);
                }
            }
        }
    });

    return consolidatedMessage;
}

function estimateTokens(messages) {
    // Estimate tokens based on character count
    return messages.reduce((acc, msg) => acc + (msg.content ? msg.content.length : 0), 0) / CHARACTERS_PER_TOKEN;
}

async function generateAssistantResponse(userMessage) {
    const chatSession = chats[currentChatId];
    const configJson = readConfigJson();
    if (!chatSession) return;
    const model = 'gpt-3.5-turbo-0613';

    try {
        const assistantMessage = await createChatCompletionSimple(model, [{ "role": "system", "content": "you must answer in fewer than 10 words, keep it short" + ( configJson.personnality || "You are a helpful assistant" )},{ role: 'user', content: userMessage }]);
        return assistantMessage;
    } catch (error) {
        console.error('Error generating assistant response:', error);
        return null;
    }
}
async function generateAssistantResponseWeather(userMessage) {
    const chatSession = chats[currentChatId];
    const configJson = readConfigJson();
    if (!chatSession) return;
    const model = 'gpt-3.5-turbo-0613';

    try {
        const assistantMessage = await createChatCompletionSimple(model, [{ "role": "system", "content": (configJson.personnality || "You are a helpful assistant" )},{ role: 'user', content: userMessage }]);
        return assistantMessage;
    } catch (error) {
        console.error('Error generating assistant response:', error);
        return null;
    }
}

// Add this code after your existing JavaScript functions
// Add this code after your existing JavaScript functions
const regenerateButton = document.getElementById('regenerate-button'); // Update the button ID
regenerateButton.addEventListener('click', async () => {
    // Get the user's last message
    const chatSession = chats[currentChatId];
    if (!chatSession) return;
    const lastUserMessage = chatSession.messages
        .filter(message => message.role === 'user')
        .pop();

    if (!lastUserMessage) {
        alert('No user message found to regenerate the bot response.');
        return;
    }

    // Find the last bot response in the chat
    const lastBotResponseIndex = chatSession.messages
    .findLastIndex(message => message.role === 'assistant');

if (lastBotResponseIndex !== -1) {
    // Remove the previous bot response from the chat data structure
    chatSession.messages.splice(lastBotResponseIndex, 1);

    // Remove the corresponding message element from the chat display
    const chatContainer = document.getElementById('chat-container');
    if (chatContainer.lastChild) {
        chatContainer.removeChild(chatContainer.lastChild);
    } else {
        console.error("No last child element in chatContainer to remove");
    }
} else {
    console.error("No last bot message found in chatSession.messages");
}

    // Regenerate the bot response
    const regeneratedResponse = await addAssistantMessageToChat(lastUserMessage.content);

    if (regeneratedResponse) {
        // Display the regenerated response in the chat
        displayMessage('assistant', regeneratedResponse, currentChatId);
        // Save the chat to localStorage with the new response
        saveChatsToLocalStorage();
    }
});



function saveChatsToLocalStorage() {
    localStorage.setItem('chats', JSON.stringify(chats));
}

function loadChatsFromLocalStorage() {
    const savedChats = localStorage.getItem('chats');
    if (savedChats) {
        chats = JSON.parse(savedChats);
        for (const chatId in chats) {
            updateChatSessions(chatId);
        }
    }
}



function initChat() {
    loadChatsFromLocalStorage();

    if (Object.keys(chats).length === 0) {
        createNewChatSession();
    } else {
        const lastChatId = Object.keys(chats)[Object.keys(chats).length - 1];
        switchChatSession(lastChatId);
    }

    // Assign colors to existing chat sessions
    assignColorsToChatSessions();
}

module.exports = {
    initChat,
    displayMessage,
    addUserMessageToChat,
    addAssistantMessageToChat,
    updateChatSessions,
    switchChatSession,
    createNewChatSession,
    updateLastMessage,
    deleteChatSession,
    generateAssistantResponse,
    generateAssistantResponseWeather
};