
const {
    addUserMessageToChat,
    addAssistantMessageToChat,
    createNewChatSession,
    generateAssistantResponse,
    generateAssistantResponseWeather,
} = require('./chat');
const { setAPIKey } = require('./api');
const { ipcRenderer } = require('electron');
const { fetchWeatherDescription } = require('./weather')
const cmd = require('node-cmd');
const os = require('os');

function saveAPIKeyToLocalStorage(apiKey) {
    localStorage.setItem('apiKey', apiKey);
}
let intervalId = null;




const snippets = [
    " call me",
    " say something fun",
    " ask me a question.",
    " say something flirty",
    " say that you're bored",
    " say something about your situation",
    " ask if everything is okay",
    " make fun of something",
    " point something out"
];
  

function formatDateToText(dateString) {
    const months = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
  
    const date = new Date(dateString);
    const day = date.getDate();
    const month = months[date.getMonth()];
  
    // Function to add ordinal suffix to the day (e.g., 12th, 23rd)
    function addOrdinalSuffix(day) {
      if (day >= 11 && day <= 13) {
        return day + 'th';
      }
      switch (day % 10) {
        case 1:
          return day + 'st';
        case 2:
          return day + 'nd';
        case 3:
          return day + 'rd';
        default:
          return day + 'th';
      }
    }
  
    const formattedDate = `the ${addOrdinalSuffix(day)} of ${month}`;
    return formattedDate;
  }
  
  const getActiveWindowInfo = () => {
    return new Promise((resolve, reject) => {
      cmd.run(`window_id=$(xprop -root _NET_ACTIVE_WINDOW | awk '{print $NF}') && xprop -id $window_id`, (err, data, stderr) => {
        if (err) {
          return reject(err);
        }
        // You can parse the 'data' string to extract specific properties
        // For example, to get the window's name:
        const nameMatch = data.match(/_NET_WM_NAME\(UTF8_STRING\) = "(.*)"/);
        const name = nameMatch ? nameMatch[1] : 'Unknown';
  
        // To get the window's class:
        const classMatch = data.match(/WM_CLASS\(STRING\) = "(.*)"/);
        let wmClass = classMatch ? classMatch[1] : 'Unknown';

         // Keep only the first part of the class
         wmClass = wmClass.split(',')[0].trim().replace(/"/g, ''); 
  
        resolve({ name, wmClass });
      });
    });
  };

  const checkSystemUsage = () => {
    const freeMemory = os.freemem();
    const totalMemory = os.totalmem();
    const cpuInfo = os.cpus();
  
    const freeMemoryInMB = (freeMemory / 1024 / 1024).toFixed(2);
    const totalMemoryInMB = (totalMemory / 1024 / 1024).toFixed(2);
    const memoryUsage = ((1 - freeMemory / totalMemory) * 100).toFixed(2);
  
    // Simplified CPU usage calculation, for demonstration purposes
    const cpuUsage = cpuInfo.reduce((acc, cpu) => acc + cpu.times.user, 0) / cpuInfo.length;
  
    return {
      freeMemory: freeMemoryInMB,
      totalMemory: totalMemoryInMB,
      memoryUsage: memoryUsage,
      cpuUsage: cpuUsage
    };
  };

ipcRenderer.on('model-query', async (event) => {
    console.log("Received load");

    // Function to generate and send the assistant message
    const sendAssistantMessage = async () => {
        const randomSnippet = snippets[Math.floor(Math.random() * snippets.length)];
        const assistantMessage = await generateAssistantResponse(randomSnippet);
        console.log(randomSnippet);

        ipcRenderer.send('model-message', assistantMessage);
    };
    const sendWeatherMessage = async () => {
        const weatherDesc = await fetchWeatherDescription();
        const weather = weatherDesc.current_condition[0].weatherDesc[0].value
        const temperature = weatherDesc.weather[0].avgtempC
        const date = formatDateToText(weatherDesc.current_condition[0].localObsDateTime)
        const weatherMessage = await generateAssistantResponseWeather("You are my assistant, deliver the information but act natural, you must keep it within 25 words: today is" + date + ",the weather is going to be" + weather+ " with temperatures around the" + temperature + "°C.");
        ipcRenderer.send('model-message', weatherMessage);
    };
    const sendWindowMessage = async () => {
        const info = await getActiveWindowInfo();

        const windowMessage = await generateAssistantResponse("I am on my computer, I am opening a "+ info.wmClass + " window. Keep it within 25 words");
        console.log(info.wmClass)
        console.log(windowMessage)
        ipcRenderer.send('model-message', windowMessage);
    };
/*     const sendSpecMessage = async () => {
        const systemInfo = checkSystemUsage()
        const specMessage = await generateAssistantResponse("I am on my computer "+ systemInfo.memoryUsage + "% of RAM is used at the moment.");
        console.log(`Free Memory: ${systemInfo.freeMemory} MB`);
        console.log(`Total Memory: ${systemInfo.totalMemory} MB`);
        console.log(`Memory Usage: ${systemInfo.memoryUsage}%`);
        console.log(`CPU Usage (simplified): ${systemInfo.cpuUsage}`);
        ipcRenderer.send('model-message', specMessage);
    }; */
    //sendSpecMessage()
    sendAssistantMessage();
    setTimeout(sendWeatherMessage, 15000);
    setTimeout(sendWindowMessage, 5000);
    // Set up an interval to send the message every 10 to 20 seconds
    intervalId = setInterval(sendAssistantMessage, 300000 + Math.random() * 120000);
    intervalId = setInterval(sendWeatherMessage, 5 * 60 * 60 * 1000);
    intervalId = setInterval(sendWindowMessage, 20*60*1000);
});


ipcRenderer.on('chat-visible', async (event) => {
    const assistantMessage = await generateAssistantResponse(" it's chitchat time");
    ipcRenderer.send('chat-message', assistantMessage);
});

ipcRenderer.on('settings-visible', async (event) => {
    const assistantMessage = await generateAssistantResponse("I'll be right back");
    ipcRenderer.send('settings-message', assistantMessage);
});

ipcRenderer.on('music-visible', async (event) => {
  const assistantMessage = await generateAssistantResponse("I'll put some music on");
  ipcRenderer.send('music-message', assistantMessage);
});

function initUI() {
    const modelSelect = document.getElementById('model-select');
    const apiKeyInput = document.getElementById('api-key-input');
    const apiKeySubmit = document.getElementById('api-key-submit');
    const userInput = document.getElementById('user-input');
    const sendButton = document.getElementById('send-button');
    const chatContainer = document.getElementById('chat-container');
    const newChatSessionButton = document.getElementById('new-chat-session-button');

    apiKeySubmit.addEventListener('click', () => {
        const apiKey = apiKeyInput.value.trim();
        if (!apiKey) return;

        setAPIKey(apiKey);
        saveAPIKeyToLocalStorage(apiKey);

        // Show a success message or any other indication that the key is set
        alert('API key set successfully');
    });

    sendButton.addEventListener('click', async () => {
        const message = userInput.value;

        if (!message) return;

        // Clear the input field
        userInput.value = '';

        // Add user message to chat container and message history
        addUserMessageToChat(message);

        // Get assistant message
        const assistantMessage = await addAssistantMessageToChat(message);
    });

    newChatSessionButton.addEventListener('click', () => {
        createNewChatSession();
    });

    userInput.addEventListener('keydown', (event) => {
        if (event.key === 'Enter') {
            if (!event.shiftKey) {
                event.preventDefault(); // Prevent newline from being added to the input
                sendButton.click();
            } else {
                userInput.value += '\n';
                event.preventDefault();
            }
        }
    });
}

module.exports = {
    initUI,
};