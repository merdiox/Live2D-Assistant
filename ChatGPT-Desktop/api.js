
const { Configuration, OpenAIApi } = require('openai');

let openai = null;

// Set the API key for OpenAI
async function setAPIKey(apiKey) {
    const configuration = new Configuration({
        apiKey,
    });

    openai = new OpenAIApi(configuration);
}

// Create a chat completion using OpenAI API
async function createChatCompletion(model, messages, onResponse) {
    const url = `https://api.openai.com/v1/chat/completions`;
    const headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${openai.configuration.apiKey}`,
    });

    const requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify({ model, messages, stream: true }),
    };

    try {
        const response = await fetch(url, requestOptions);

        if (!response.ok) {
            const errorDetails = await response.json(); // Assuming OpenAI returns the error details in a JSON format
            console.error('Detailed Error:', errorDetails);
            throw new Error(`Error response from OpenAI: ${response.statusText}`);
        }

        const reader = response.body.getReader();
        let buffer = '';

        while (true) {
            const { done, value } = await reader.read();
            if (done) {
                break;
            }

            buffer += new TextDecoder('utf-8').decode(value, { stream: true });
            
            if (buffer.includes('[DONE]')) {
                break;
            }

            buffer = processPayloads(buffer, onResponse);
        }
    } catch (error) {
        console.error('Error calling the OpenAI API:', error);
    }
}

function processPayloads(buffer, onResponse) {
    const payloads = buffer.split('\n\n');
    buffer = payloads.pop();

    for (const payload of payloads) {
        if (payload.startsWith('data:')) {
            const data = payload.slice(5).trim();

            try {
                const delta = JSON.parse(data);
                if (delta.choices?.[0]?.delta?.content) {
                    onResponse(delta.choices[0].delta.content);
                }
            } catch (error) {
                console.error('Error parsing JSON:', error.message, error);
            }
        }
    }

    return buffer;
}



async function createChatCompletionSimple(model, messages) {
    if (!openai) {
        alert('Please set your API key first.');
        return null;
    }

    const url = `https://api.openai.com/v1/chat/completions`;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', `Bearer ${openai.configuration.apiKey}`);

    const requestOptions = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify({ model, messages, stream: false }),
    };

    try {
        const response = await fetch(url, requestOptions);
        const data = await response.json();

        if (data.choices && data.choices.length > 0) {
            const answer = data.choices[0].message.content;
            return answer;
        } else {
            return null;
        }

    } catch (error) {
        console.error('Error calling the OpenAI API:', error);
    }
}

// Initialize the API
function initAPI() {
    const apiKey = localStorage.getItem('apiKey');
    if (apiKey) {
        setAPIKey(apiKey);
        const apiKeyInput = document.getElementById('api-key-input');
        apiKeyInput.value = apiKey;
    }
}

module.exports = {
    setAPIKey,
    createChatCompletion,
    initAPI,
    createChatCompletionSimple,
};