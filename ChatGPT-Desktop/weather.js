const https = require('https');

function fetchWeatherDescription() {
  return new Promise((resolve, reject) => {
    const url = `https://wttr.in/?format=j1`;

    https.get(url, (response) => {
      let data = '';

      response.on('data', (chunk) => {
        data += chunk;
      });

      response.on('end', () => {
        try {
          const weatherData = JSON.parse(data);
          const currentWeatherDesc = weatherData
          resolve(currentWeatherDesc);
        } catch (error) {
          reject(error);
        }
      });
    }).on('error', (error) => {
      reject(error);
    });
  });
}

module.exports = {
    fetchWeatherDescription,
};