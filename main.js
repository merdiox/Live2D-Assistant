const { error } = require('console');
const fs = require('fs');
const { app, BrowserWindow, screen, ipcMain, Tray, Menu, shell, dialog} = require('electron');
console.log('Electron version:', process.versions.electron);
const path = require('path');
const url = require('url');
const cmd = require('node-cmd');
let x = 0; // Initialize x and y
let y = 0;
let chatWindow;
let mainWindow;
let musicWindow = null;

process.on('uncaughtException', (error) => {
  console.error("Uncaught Exception: ", error);
});
process.on('unhandledRejection', (reason, promise) => {
  console.error('Unhandled Rejection at:', promise, 'reason:', reason);
});


function createHiddenChatWindow() {
  chatWindow = new BrowserWindow({
      width: 1200,
      height: 800,
      webPreferences: {
          nodeIntegration: true,
          contextIsolation: false,
      },
      show: false, // The window is initially hidden
      frame: true,
      x: 140,
      y: 150,
  });

  chatWindow.loadURL(url.format({
      pathname: path.join(__dirname, '/ChatGPT-Desktop/index.html'),
      protocol: 'file:',
      slashes: true,
  }));

  chatWindow.on('closed', () => {
      chatWindow = null;
  });

  //chatWindow.webContents.openDevTools();
}

function createHiddenMusicWindow() {
  if (musicWindow !== null) return;
  musicWindow = new BrowserWindow({
      width: 1200,
      height: 800,
      webPreferences: {
          nodeIntegration: true,
          contextIsolation: false,
      },
      show: false,
      frame: true,
      x: 140,
      y: 150,
      icon: path.join(__dirname, 'music.png'),
  });

  musicWindow.loadURL(url.format({
      pathname: path.join(__dirname, '/Music/index.html'),
      protocol: 'file:',
      slashes: true,
  }));

  musicWindow.on('closed', () => {
    musicWindow = null;
  });

  //musicWindow.webContents.openDevTools();
  shouldStartShuffle = true;
}

function toggleMusicWindow() {
  if (musicWindow) {
    musicWindow.close();
    musicWindow = null;
  } else {
    createHiddenMusicWindow();
  }
}

function createCustomBrowserWindow() {
  const displays = screen.getAllDisplays();
  ipcMain.handle('getScreenInfo', () => {
    return screen.getCursorScreenPoint();
  });

  const mainWindow = new BrowserWindow({
    x: displays[0].bounds.x, // Starting from the leftmost screen
    y: displays[0].bounds.y,
    width: displays.reduce((totalWidth, display) => totalWidth + display.bounds.width, 0), // Combined width of all screens
    height: Math.max(...displays.map((display) => display.bounds.height)), // Maximum height among all screens
    transparent: true,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, 'preload.js'),
    },
    frame: false,
    alwaysOnTop: true,
    focusable: false,
    skipTaskbar: true,
  });

  return mainWindow;
}



app.on('ready', () => {
  console.log("App is ready");
  const mainScreen = screen.getPrimaryDisplay();
  const taskbarBounds = mainScreen.workArea;
  const iconPath = path.join(__dirname, '256icon.png');
  tray = new Tray(iconPath);

  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Quit',
      click: () => {
        app.quit(); // Quit the app when the "Quit" option is selected
      }
    },
    {
      label: 'Save position',
      click: () => {
        mainWindow.webContents.send('save-pos')
      }
    },
    {
      label: 'Reload',
      click: () => {
        mainWindow.webContents.reloadIgnoringCache()
      }
    }
  ]);

  tray.setContextMenu(contextMenu);

  mainWindow = createCustomBrowserWindow(true);
  console.log("Main window created", !!mainWindow);
  mainWindow.setBounds({
    x: taskbarBounds.x,
    y: taskbarBounds.y + taskbarBounds.height - mainWindow.getBounds().height,
  });
  mainWindow.webContents.setWindowOpenHandler(({ url }) => {
    if (url.endsWith("ChatGPT-Desktop/index.html")) {
      const screenWidth = mainScreen.bounds.width;
      return {
        action: 'allow',
        overrideBrowserWindowOptions: {
          width: Math.floor(screenWidth / 2), // Set the width to half of the screen width
          height: mainScreen.bounds.height,
          webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
          },
          show: true,
          frame: true,
          x: 0, // Set the x-coordinate to 0
          y: 0, // Set the y-coordinate to 0
          icon: path.join(__dirname, '256icon.png'),
        },
      };
    } else if (url.endsWith("Settings/index.html")) {
      return {
        action: 'allow',
        overrideBrowserWindowOptions: {
          width: 300,
          height: 600,
          webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
          },
          show: true,
          frame: true,
          x: 140,
          y: 150,
          icon: path.join(__dirname, 'setting.png'),
        },
      };
    } else if (url.endsWith("Music/index.html")) {
      return {
        action: 'allow',
        overrideBrowserWindowOptions: {
          width: 300,
          height: 600,
          webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
          },
          show: true,
          frame: true,
          x: 140,
          y: 150,
          icon: path.join(__dirname, 'music.png'),
        },
      };
    } else {
      return { action: 'deny' };
    }
  });
  
  
  const configData = readConfigFile();
  if (configData !== null) {
    // Check if the mainWindow is ready before sending the message
    if (mainWindow && !mainWindow.isDestroyed() && mainWindow.webContents) {
      // Wait for the renderer process to finish loading
      mainWindow.webContents.once('did-finish-load', () => {

              createHiddenChatWindow();
              chatWindow.once('ready-to-show', () => {

              chatWindow.webContents.send('model-query')
              console.log("sent")
              ipcMain.on('chat-created', (event) => {
                chatWindow.webContents.send('chat-visible');  
              });
              ipcMain.on('settings-created', (event) => {
                chatWindow.webContents.send('settings-visible');  
              });
              ipcMain.on('music-created', (event) => {
                chatWindow.webContents.send('music-visible');  
              });
            
              })
            })
    } else {
      console.error('Main window is not available or destroyed.');
    }
  } else {
    mainWindow.webContents.send('file-path', 'downloaded_files/date_masamune_ur/live2d.model3.json');
  }
  const timer = setInterval(() => {
    if (mainWindow && !mainWindow.isDestroyed()) {
    const point = screen.getCursorScreenPoint();
    const [x, y] = mainWindow.getPosition();
    const [w, h] = mainWindow.getSize();

    if (mainWindow && !mainWindow.isDestroyed()) {
    if (point.x > x && point.x < x + w && point.y > y && point.y < y + h) {
      updateIgnoreMouseEvents(point.x - x, point.y - y);
    }
  }
  }
}, 5);

  const updateIgnoreMouseEvents = async (x, y) => {
    if (mainWindow && !mainWindow.isDestroyed()) {
      // Check if the mainWindow exists and is not destroyed
      const image = await mainWindow.webContents.capturePage({
        x,
        y,
        width: 1,
        height: 1,
      });
      mainWindow.webContents.send('return-cursor-position', [x,y]);
      const buffer = image.getBitmap();
  
      if (mainWindow && !mainWindow.isDestroyed()) {
        mainWindow.setIgnoreMouseEvents(!buffer[3]);
      }
    }
  };
  
  // Maximize the window
  mainWindow.maximize();


  
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

  function readConfigFile() {
    try {
      // Read the JSON file synchronously and parse its content
      const configJson = fs.readFileSync(path.join(__dirname, 'config.json'), 'utf8');
      const configData = JSON.parse(configJson);

      return configData;
    } catch (error) {
      // Handle any errors that may occur during file reading or parsing.
      console.error('Error reading config.json:', error.message);
      return null; // Return null or handle the error in a different way as needed.
    }
  }

ipcMain.on('did-finish-load', () => {
    if (shouldStartShuffle) {
      musicWindow.webContents.send('populate-playlist'); // Assuming you have logic to handle this
    }
  });

  ipcMain.on('playlist-populated', () => {
    if (shouldStartShuffle) {
      musicWindow.webContents.send('shuffle-command');
      shouldStartShuffle = false;  // Reset flag
    }
  });

ipcMain.on('model-message', (event, reply) => {
  mainWindow.webContents.send('model-show', reply);  
});
ipcMain.on('chat-message', (event, reply) => {
  mainWindow.webContents.send('chatmsg-show', reply);  
});
ipcMain.on('settings-message', (event, reply) => {
  mainWindow.webContents.send('setmsg-show', reply);  
});
ipcMain.on('music-message', (event, reply) => {
  mainWindow.webContents.send('setmsc-show', reply);  
});

ipcMain.on('toggle-music-window', toggleMusicWindow);

ipcMain.on('skip-song', () => {
  musicWindow.webContents.send('skip-command');
});

ipcMain.on('file-request', (event) => {  
  if (process.platform !== 'darwin') {
    dialog.showOpenDialog({
      title: 'Select the Music Folder',
      properties: ['openDirectory']
    }).then(file => {
      // Stating whether dialog operation was
      // cancelled or not.
      console.log(file.canceled);
      if (!file.canceled) {
        const filepath = file.filePaths[0].toString();
        console.log(filepath);
        event.reply('file', filepath);
      }  
    }).catch(err => {
      console.log(err)
    });
  }
});


ipcMain.on('write-json', (event, data) => {
  console.log(data)
  const filePath = data[1];
  const fileDir = path.dirname(filePath);
  const saveFilePath = path.join(__dirname, path.join(path.dirname(filePath), 'model_position.json'));
  console.log(saveFilePath)

  let existingData = {};
  try {
    const existingDataStr = fs.readFileSync(saveFilePath, 'utf8');
    existingData = JSON.parse(existingDataStr);
  } catch (err) {
    // Handle errors if the file doesn't exist or is invalid JSON
  }

  // Update the position and zoom values
  const positionArr = data[0];
  const x = positionArr[0];
  const y = positionArr[1];
  const zoom = positionArr[2];

  // Merge the new data with the existing data
  const saveData = {
    ...existingData,
    x,
    y,
    zoom,
  };

  // Write the merged data back to the JSON file
  fs.writeFileSync(saveFilePath, JSON.stringify(saveData, null, 2), 'utf8');
});

ipcMain.on('model-reload', (event) => {
  console.log('relaozd')
  mainWindow.webContents.reload();
});

  // Open the DevTools for debugging (you can remove this line in production)
  //mainWindow.webContents.openDevTools({ mode: 'detach' });
  

});

