document.addEventListener('DOMContentLoaded', () => {
    const openMenu = document.getElementById('canvas-1');
    const chatButton = document.getElementById('openChatButton');
    const settingsButton = document.getElementById('wrench-button');
    const musicButton = document.getElementById('music-button');
    const textSpace = document.getElementById('textSpace');

    chatButton.style.display = 'none';
    settingsButton.style.display = 'none';
    musicButton.style.display = 'none';

    const buttons = [chatButton, settingsButton, musicButton]
    const openWindows = [];

    openMenu.addEventListener('contextmenu', (event) => {
      event.preventDefault();  // Prevent the default right-click menu from showing
      electron.ipcRenderer.send('skip-song');  // Send the IPC message to skip the song
  });

    electron.ipcRenderer.on('model-show', (event, message) => {
      typeText(message, textSpace);
      });

    electron.ipcRenderer.on('chatmsg-show', (event, message) => {
    typeText(message, textSpace);
    });

    electron.ipcRenderer.on('setmsg-show', (event, message) => {
      typeText(message, textSpace);
    });
    
    electron.ipcRenderer.on('setmsc-show', (event, message) => {
      typeText(message, textSpace);
    });

      function typeText(message, object) {
        const messageF = emojiStrip(message);
        const messageFX = messageF.replace(/FIRST/g, '');
        console.log(messageFX);
        const words = messageF.split(' ');
        let index = 0;
        object.style.display = 'block';
    
        // Calculate the duration based on a reference rate of 260 words per minute
        const wordsPerMinute = 200;
        const minDisplayDuration = 3000; // 4 seconds in milliseconds
        const maxDisplayDuration = 12000; // 15 seconds in milliseconds
        const estimatedDuration = (words.length / wordsPerMinute) * 60 * 1000;
        const displayDuration = Math.min(
            Math.max(estimatedDuration, minDisplayDuration),
            maxDisplayDuration
        );
    
        function addWord() {
            if (index < words.length) {
                object.textContent += words[index] + ' ';
                index++;
                setTimeout(addWord, 5); // Adjust the delay as needed (1 second in this example)
            } else {
                // Text is fully typed, now set a timeout to remove it
                setTimeout(() => {
                    object.textContent = '';
                    object.style.display = 'none';
                }, displayDuration);
            }
        }
    
        addWord();
    }
    
    chatButton.addEventListener('click', () => {
      // Check if a chat window is already open
      const isChatWindowOpen = openWindows.includes('chatWindow');
      
      if (!isChatWindowOpen) {
        const chatWindow = window.open('ChatGPT-Desktop/index.html', 'chatWindow');
        electron.ipcRenderer.send('chat-created');
        openWindows.push('chatWindow');
        
        for (const button of buttons) {
          button.style.display = 'none';
        }
      }
    });

    musicButton.addEventListener('click', () => {
      electron.ipcRenderer.send('toggle-music-window');
      const isMusicWindowOpen = openWindows.includes('settingsWindow');
      
      if (!isMusicWindowOpen) {
        openWindows.push('musicWindow');
        electron.ipcRenderer.send('music-created');
        
        for (const button of buttons) {
          button.style.display = 'none';
        }
      }
  });
    
    settingsButton.addEventListener('click', () => {
      // Check if a settings window is already open
      const isSettingsWindowOpen = openWindows.includes('settingsWindow');
      
      if (!isSettingsWindowOpen) {
        const settingsWindow = window.open('Settings/index.html', 'settingsWindow');
        electron.ipcRenderer.send('settings-created');
        openWindows.push('settingsWindow');
        
        for (const button of buttons) {
          button.style.display = 'none';
        }
      }
    });
    
  
  openMenu.addEventListener('click', () => {
    let anyWindowOpen = false;
  
    for (const win of openWindows) {
      if (!win || win.closed) {
        // If a window is closed or doesn't exist, remove it from the array
        openWindows.splice(openWindows.indexOf(win), 1);
      } else {
        anyWindowOpen = true;
        break; // No need to check further, at least one window is open
      }
    }
  
    if (!anyWindowOpen) {
      // Check if the buttons are currently visible
      const buttonsVisible = Array.from(buttons).every((button) => {
        return button.style.display === 'block';
      });
  
      if (!buttonsVisible) {
        // If buttons are not visible, show them
        for (const button of buttons) {
          button.style.display = 'block';
        }
      } else {
        // If buttons are already visible, hide them
        for (const button of buttons) {
          button.style.display = 'none';
        }
      }
    } else {
      openWindows.length = 0; // Clear the array
      for (const button of buttons) {
        button.style.display = 'none';
      }
    }
  });
  

  electron.ipcRenderer.on('return-cursor-position', (event,position) => {
    var positionInfoElement = document.getElementById('cursorCoordinates');
    positionInfoElement.textContent = position;});


});